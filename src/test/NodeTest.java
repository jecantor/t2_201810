package test;

import static org.junit.Assert.*;

import org.junit.*;

import com.sun.xml.internal.messaging.saaj.packaging.mime.util.QDecoderStream;

import model.data_structures.Node;

public class NodeTest 
{
	private Node <Integer> x;
	
	private Node <Integer> x2;
	
	private Node <Double> y;
	
	private Node <Double> y2;
	
	private Node <Boolean> z;
	
	private Node <Boolean> z2;
	
	@Before
	public void setupScenario()
	{
		x = new Node <Integer> (new Integer (4));
		
		x2 = new Node <Integer> (new Integer (5));
		
		y = new Node <Double> (new Double (0.5));
		
		y2 = new Node <Double> (new Double (1.5));
		
		z = new Node <Boolean> (new Boolean (true));
		
		z2 = new Node <Boolean> (new Boolean (true));
		
	}
	
	@Test
	public void testChangeNext()
	{
		x.changeNext(x2);
		y.changeNext(y2);
		z.changeNext(z2);
		
		assertTrue("The next element is incorrectly", x.giveNext() == x2);
		assertTrue("The next element is incorrectly", y.giveNext() == y2);
		assertTrue("The next element is incorrectly", z.giveNext() == z2);

	}
	
	public void testDeleteNext()
	{
		x.deleteNext();
		y.deleteNext();
		z.deleteNext();
		
		assertTrue("The next element is incorrectly", x.giveNext() == null);
		assertTrue("The next element is incorrectly", y.giveNext() == null);
		assertTrue("The next element is incorrectly", z.giveNext() == null);

	}
	

}
