package test;

import org.junit.*;

import model.data_structures.List;
import model.data_structures.Node;

import static org.junit.Assert.*;

public class ListTest
{
	private List <Integer> myIntList;
	
	private Node <Integer> n1;

	private Node <Integer> n2;
	
	private Node <Integer> n3;
	
	@Before
	public void setupScenario()
	{
		myIntList = new List <Integer>();
		
		n1 = new Node <Integer> (new Integer(2));

		n2 = new Node <Integer> (new Integer(3));
		
		n3 = new Node <Integer> (new Integer(4));
	}
	
	public void setupScenario2()
	{
		
		myIntList.addElement(n1);
		myIntList.addElement(n2);
		myIntList.addElement(n3);
	}
	
	@Test
	public void addElementTest()
	{
		assertNull("The head should be null",myIntList.head);
		
		setupScenario2();
		assertNotNull("The head should not be null",myIntList.head);
		assertEquals("The list size is incorrect", 3, myIntList.getSize());
		assertEquals("The list added the elements incorrectly",n1, myIntList.getElementByPosition(1));
		assertEquals("The list added the elements incorrectly", n3.getValue(), myIntList.getElementByPosition(3).getValue());
	}
	
	public void deleteElementTest()
	{
		
		setupScenario2();
		assertEquals("The list size is incorrect", 4, myIntList.getSize());
		assertTrue("The list added the elements incorrectly", myIntList.getElementByPosition(1) == n1);
		assertTrue("The list added the elements incorrectly", myIntList.getElementByPosition(3) == n3);
		
		assertTrue("It should have deleted the element",myIntList.deleteElement(n1));
		assertFalse("It should have not deleted the element",myIntList.deleteElement(n1));
		myIntList.deleteElement(n2);
		myIntList.deleteElement(n3);
		assertEquals("The list size is incorrect",0,myIntList.getSize());
	}
	
	public void getSizeTest()
	{
		assertEquals("The size list is incorrectly",0, myIntList.getSize());
		
		setupScenario2();
		assertEquals("The size list is incorrectly",3, myIntList.getSize());
	}

}
