package model.data_structures;

public class Node <T extends Comparable <T>>
{
	public Node <T> next;
	
	public Node <T> previous;
	
	public T value;
	
	public Node (T o)
	{
		value = o;
		next = null;
		previous = null;
		
	}
	
	public void changeNext (Node<T> obj)
	{
		next = obj;
	}
	
	public void deleteNext()
	{
		next = null;
	}
	
	public Node <T> giveNext()
	{
		return next;
	}
	
	public Node <T> givePrevious()
	{
		return previous;
	}
	
	public T getValue()
	{
		return value;
	}

}
