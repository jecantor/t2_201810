package model.data_structures;

public class List <T extends Comparable <T>> implements LinkedList<T>
{
	public int size;
	
	public Node <T> head;
	
	public Node <T> actual;
	
	public List()
	{
		head = null;
		size = 0;
	}

	@Override
	public void addElement(Node <T> element)
	{
		if (getElement(element) != null)
			return;
		
		if (head == null)
		{
			head = element;
			size = 1;
		}
		
		else
		{
		
		Node <T> temp = head;
			
		while (temp.next != null)
		{
			temp = temp.next;
		}
		
		temp.changeNext(element);
		size++;
		
		}

		
		
	}

	@Override
	public boolean deleteElement(Node<T> element)
	{
		
		if (head == null)
		{
			return false;
		}
		
		else if (head.equals(element))
		{
			head = head.next;
			size--;
			return true;
		}
		
		else
		{
			Node <T> temp = head;
			
			while (temp.next != null)
			{
				if (temp.next.getValue().compareTo(element.getValue()) == 0)
				{
					temp.changeNext(temp.next.next);
					size--;
					return true;
				}
				
				temp = temp.next;
				
			}
		}

		return false;
	}

	@Override
	public Node <T> getElement(Node<T> element)
	{
		if (head == null)
		{
			return null;
		}
		
		else if (head.getValue().compareTo(element.getValue()) == 0)
		{
			return head;
		}
		
		else
		{
			Node <T> temp = head;
			
			while (temp.next != null)
			{
				if (temp.getValue().compareTo(element.getValue()) == 0)
				{
					return temp;
				}
				
				temp = temp.next;
			}
		}
		return null;
	}

	@Override
	public int getSize()
	{
		return size;
	}

	@Override
	public Node <T> getElementByPosition(int pos)
	{
		System.out.println("Pos------ " + pos);
		
		int p = 1;
		
		if (pos == 0)
		{
			return head;
		}
		
		else
		{
		
			Node <T> temp = head;
			
			while (temp != null)
			{
				if (pos == p)
				{
					return temp;
				}
				
				p++;
				
				temp = temp.next;
			}
		}
		
		System.out.println("I came to this point");
		return null;
	}

	@Override
	public void listing(List<T> list)
	{
		if (head == null)
		{
			head = list.getElementByPosition(0);
		}
		
		else
		{
			Node <T> temp = head.next;
			
			list.getElementByPosition(list.getSize()).changeNext(temp);
			
			head = list.getElementByPosition(0);	
			
		}
		
		
	}
	

	
	public Node <T> giveHead()
	{
		return head;
	}

	@Override
	public Node <T> getCurrent()
	{
		return actual;
	}

	@Override
	public void nextElement() {
		// TODO Auto-generated method stub
		
	}

}
