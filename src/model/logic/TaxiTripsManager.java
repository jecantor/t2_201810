package model.logic;

import api.ITaxiTripsManager;
import model.vo.Taxi;
import model.vo.Service;
import model.data_structures.LinkedList;
import model.data_structures.List;
import model.data_structures.Node;

import java.io.*;

import com.google.gson.*;

public class TaxiTripsManager implements ITaxiTripsManager {

	private List <Taxi> taxiList;
	
	private List <Service> serviceList;
	
	public TaxiTripsManager()
	{
		taxiList = new List <Taxi>();
		serviceList = new List <Service>();
	}
	// TODO
	// Definition of data model 
	
	public void loadServices (String serviceFile)
	{
	

		JsonParser parser = new JsonParser();
				
		try {
					
		/* Cargar todos los JsonObject (servicio) definidos en un JsonArray en el archivo */
		JsonArray arr= (JsonArray) parser.parse(new FileReader(serviceFile));
					
			/* Tratar cada JsonObject (Servicio taxi) del JsonArray */
		for (int i = 0; arr != null && i < arr.size(); i++)
		{
		JsonObject obj= (JsonObject) arr.get(i);
		/* Mostrar un JsonObject (Servicio taxi) */
		System.out.println("------------------------------------------------------------------------------------------------");
		System.out.println(obj);
		
		
		String taxiId = "NaN";
		
		if (obj.get("taxi_id") != null)
		{taxiId = obj.get("taxi_id").getAsString();}
		
		String company = "NaN";
		
		if (obj.get("company") != null)
		{company = obj.get("company").getAsString();}
		
		
		if (!taxiId.equals("NaN") && !company.equals("NaN"))
		{
		Node <Taxi> newNode = new Node <Taxi> (new Taxi (taxiId,company));
		taxiList.addElement(newNode);
		}
		
		//System.out.println("Tama�o de la lista de taxis: ---- " + taxiList.getSize());
		System.out.println("Compa�ia de taxis: ---- " + taxiList.getElementByPosition(taxiList.size-1).getValue().getCompany());
		System.out.println("Taxi Id: ---- " + taxiList.getElementByPosition(taxiList.size-1).getValue().getTaxiId());
		
		
		
		String tripId = "NaN";
		
		if (obj.get("trip_id") != null)
		{tripId = obj.get("trip_id").getAsString();}

		int seconds = 0;
		
		if (obj.get("trip_seconds") != null)
		{seconds = obj.get("trip_seconds").getAsInt();}
		
		double tripMiles = 0.0;
		
		if (obj.get("trip_miles") != null)
		{tripMiles = obj.get("trip_miles").getAsDouble();}
		

		double tripTotal = 0.0;
		
		if (obj.get("trip_total") != null)
		{tripTotal = obj.get("trip_total").getAsDouble();}
		
		int communityArea = 0;
		if (obj.get("dropoff_community_area") != null)
		{communityArea = obj.get("dropoff_community_area").getAsInt();}
		
		if (!tripId.equals("NaN") && seconds != 0 && tripMiles != 0.0 && tripTotal != 0.0 && communityArea != 0)
		{
		Node <Service> newNode2 = new Node <Service> (new Service (tripId,taxiId,seconds,tripMiles,tripTotal,communityArea));
		serviceList.addElement(newNode2);}
		
		System.out.println("Tama�o de la lista de taxis: ---- " + taxiList.getSize());
		
		if(serviceList.size >= 2)
		System.out.println("Community Area Code: ---- " + serviceList.getElementByPosition(serviceList.size-1).getValue().getCommunityAreaCode());
		
		//System.out.println("Tama�o de la lista de servicios: ---- " + serviceList.getSize());
		
		

			}
			}
				catch (JsonIOException e1 ) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				catch (JsonSyntaxException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				catch (FileNotFoundException e3) {
					// TODO Auto-generated catch block
					e3.printStackTrace();
				}
			
		System.out.println("Inside loadServices with " + serviceFile);
	}

	@Override
	public LinkedList<Taxi> getTaxisOfCompany(String company) {
		// TODO Auto-generated method stub
		
		
		Node <Taxi> temp = taxiList.head;
		
		List <Taxi> search = new List <Taxi>();
		
		while (temp.next != null)
		{
			System.out.println("Compa�ias:::" + temp.getValue().getCompany());
			
			if (temp.getValue().getCompany().equalsIgnoreCase(company))
			{

				search.addElement(temp);
			}
			
			temp = temp.next;
		}
		
		System.out.println("Inside getTaxisOfCompany with " + company);
		return search;
	}

	@Override
	public LinkedList<Service> getTaxiServicesToCommunityArea(int communityArea) {
		

		
		Node <Service> temp = serviceList.head;
		
		List <Service> search = new List <Service>();
		
		
		while (temp != null)
		{
			if (temp.getValue().getCommunityAreaCode() == communityArea)
			{
				search.addElement(temp);
			}
			
			temp = temp.next;
		}
		
		// TODO Auto-generated method stub
		System.out.println("Inside getTaxiServicesToCommunityArea with " + communityArea);
		return search;
	}


}
