package model.vo;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{

	private String taxiId;
	
	private String company;
	
	public Taxi (String tId,String c)
	{
		taxiId =tId;
		company = c;
		
	}
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId()
	{
		// TODO Auto-generated method stub
		return taxiId;
	}

	/**
	 * @return company
	 */
	public String getCompany() {
		// TODO Auto-generated method stub
		return company;
	}
	
	@Override
	public int compareTo(Taxi o) {
		// TODO Auto-generated method stub
		return this.equals(o)? 0 : -1;
		

	}	
}
