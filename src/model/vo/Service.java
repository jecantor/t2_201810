package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {

	private String tripId;
	
	private String taxiId;
	
	private int seconds;
	
	private double tripMiles;
	
	private double tripTotal;
	
	private int communityArea;
	
	public Service (String tId, String tI, int s, double tM, double tT, int cA)
	{
		tripId = tId;
		taxiId = tI;
		seconds = s;
		tripMiles = tM;
		tripTotal = tT;
		communityArea = cA;
	}
	/**
	 * @return id - Trip_id
	 */
	public String getTripId()
	{
		// TODO Auto-generated method stub
		return tripId;
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxiId;
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return seconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return tripMiles;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return tripTotal;
	}
	
	public int getCommunityAreaCode() {
		
		return communityArea;
	}

	@Override
	public int compareTo(Service o) {
		// TODO Auto-generated method stub
		return this.equals(o)? 0 : -1;
	}
}
